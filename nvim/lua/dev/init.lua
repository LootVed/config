print("TODO make plugin download async")
print("Setup terrortylor/nvim-comment")
prequire("dev.go")
prequire("dev.lua")
prequire("dev.rust")
prequire("dev.cpp")
prequire("dev.jvm")

if is_dev_env() then
	prequire("dev.opt")
else
	require("plugins.lsp").setuplsp("texlab")
end

local b = require("null-ls").builtins
local f = b.formatting
local d = b.diagnostics
local custom = require("plugins.format")
local augroup = vim.api.nvim_create_augroup("LspFormatting", {})
require("null-ls").setup({
	sources = {
		f.shfmt,
		f.gofmt,
		f.stylua,
		f.goimports,
		f.clang_format,
		d.shellcheck,
		custom.anyfmt,
	},
	on_attach = function(client, bufnr)
		if client.supports_method("textDocument/formatting") then
			vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
			vim.api.nvim_create_autocmd("BufWritePre", {
				group = augroup,
				buffer = bufnr,
				callback = function()
					vim.lsp.buf.format({ bufnr = bufnr })
				end,
			})
		end
	end,
})
