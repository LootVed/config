local function get_defaults(plugin)
	assert(plugin ~= nil, "plugin parameter must be a table")
	local repos = plugin[1]
	local alias = plugin["alias"]
	local base_url = plugin["base_url"]

	assert(repos ~= nil, "Repos must be supplied as the first unamed field of the plugin argument")
	local match = string.match(repos, "/.*")
	assert(match ~= nil, "invalid plugin repos, must be in the format project/pluginName")
	if base_url == nil then
		base_url = "https://github.com/"
	end
	if alias == nil then
		alias = string.sub(match, 2, -1)
	end
	local path = os.getenv("SOFTWARE_CACHE") .. "/nvim/plugins/" .. alias
	return base_url .. repos, path, repos
end

local async = require("utils.async")

local function _git(...)
	local cmd = { "git", ... }
	return async.run(cmd)
end

local function git(path, ...)
	return _git("-C", path, ...)
end

local function clone_plugin(plugin)
	local url, path, repos = get_defaults(plugin)
	local job_id
	if vim.fn.empty(vim.fn.glob(path)) > 0 then
		print("cloning " .. repos)
		job_id = _git("clone", "--depth=1", url, path)
		local commit = plugin["commit"]
		if commit ~= nil then
			git(path, "fetch", "--depth=1", "origin", commit)
			git(path, "checkout", "FETCH_HEAD")
		end
	else
		print(repos .. " already exists")
	end
	return job_id
end

local function save_current_plugins_version()
	local dst = vim.env.NVIM_DIR .. "/lua/bootstrap/plugins.lua"
	--file, err = io.open(dst, "w")
	local plugins = require("plugins").list
	local f = io.open(dst, "w")
	assert(f ~= nil, "unable to open outfile file to save plugins versions")
	f:write("return {\n")

	for _, plugin in pairs(plugins) do
		local _, path, repos = get_defaults(plugin)
		local commit = git(path, "rev-parse", "--verify", "HEAD"):sub(0, -2)

		f:write('    {"' .. repos .. '", commit = "' .. commit .. '"},\n')
	end
	f:write("}")
	f:close()
end

local function update(plugin)
	local _, path, _ = get_defaults(plugin)
	git(path, "pull")
end

return {
	update = update,
	clone = clone_plugin,
	save_versions = save_current_plugins_version,
}
