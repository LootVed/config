local pkg = require("bootstrap.pkg")

local function run_on_all_plugin(func)
	local plugins = require("plugins").list
	local job_ids = {}
	for _, plugin in pairs(plugins) do
		local job_id = func(plugin)
		table.insert(job_ids, job_id)
	end
	require("utils.async").wait(job_ids)
end

local function init()
	run_on_all_plugin(pkg.clone)
end

local function update()
	run_on_all_plugin(pkg.update)
end

local function init_if_needed()
	local init_is_done_path = os.getenv("SOFTWARE_CACHE") .. "/nvim/plugins/init_is_done"
	local must_refresh = os.getenv("NVIM_FORCE_INIT") ~= nil
	local must_init = must_refresh or vim.fn.filereadable(init_is_done_path) == 0
	if must_init then
		if pcall(init) then
			os.execute("touch " .. init_is_done_path)
			os.execute("chmod 755 " .. init_is_done_path)
		end
	end
end

return {
	init = init,
	update = update,
	run = init_if_needed,
}
