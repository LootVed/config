local packages = {
	{ "neovim/nvim-lspconfig" },
	{ "mfussenegger/nvim-dap" },
	{ "nvim-treesitter/nvim-treesitter" },

	{ "williamboman/mason.nvim" },
	{ "williamboman/mason-lspconfig.nvim" },
	-- if no ls is available use linter and formatters
	{ "jose-elias-alvarez/null-ls.nvim" },
	-- search & jump to char
	-- { "ggandor/leap.nvim" },
	{ "ggandor/lightspeed.nvim" },

	{ "hrsh7th/cmp-path" },
	{ "hrsh7th/nvim-cmp" },
	{ "hrsh7th/cmp-buffer" },
	{ "hrsh7th/cmp-cmdline" },
	{ "hrsh7th/cmp-nvim-lsp" },

	{ "nvim-lua/completion-nvim" },
	{ "nvim-lua/lsp_extensions.nvim" },
	{ "nvim-telescope/telescope.nvim" },
	{ "nvim-telescope/telescope-ui-select.nvim" },

	{ "dcampos/nvim-snippy" },
	{ "windwp/nvim-autopairs" },
	{ "Maan2003/lsp_lines.nvim" },
	{ "lukas-reineke/indent-blankline.nvim" },
	{ "sindrets/diffview.nvim" },
	-- neovim lib required by plugins
	{ "nvim-lua/plenary.nvim" },
	-- Theme setup
	--{ "EdenEast/nightfox.nvim" },
	{ "navarasu/onedark.nvim" },
	--{ "catppuccin/nvim", alias = "catppuccin" },
	{ "hoob3rt/lualine.nvim" },
	-- Dev setup
	-- Rust lang
	{ "simrat39/rust-tools.nvim" },
}

local function setup_mason()
	require("mason").setup({
		ui = {
			icons = {
				package_installed = "✓",
				package_pending = "➜",
				package_uninstalled = "✗",
			},
		},
		install_root_dir = vim.env.SOFTWARE_CACHE .. "/nvim/mason",
	})
end

local function main()
	setup_mason()
	prequire("plugins.cfg")
	prequire("plugins.lsp")
	prequire("plugins.autocomplete")
	--require("leap").add_default_mappings()
	-- leap still bugged
end

local function install_dev_tools(tools)
	setup_mason()
	local installer = require("mason.api.command").MasonInstall
	for _, tool in pairs(tools) do
		print({ name = tool })
		-- installer({ name = tool })
	end
end

local function init_dev_env(env_type)
	local tools = { "lua-language-server", "shfmt", "shellcheck" }
	if "full" == env_type then
		tools = { "rust_analyzer", "jdtls", "delve", unpack(tools) }
	end
	install_dev_tools(tools)
end

return {
	load = main,
	list = packages,
	init_dev_env = init_dev_env,
}
