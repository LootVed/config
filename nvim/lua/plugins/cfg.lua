local function _setup_theme()
	local themes = { "onedark", "nightfox", "catppuccin", "tokyonight", "material" }
	local theme_nbr = 1
	local theme = require(themes[theme_nbr])
	if theme_nbr == 1 then
		theme.setup({
			style = "darker",
		})
		theme.load()
	elseif theme_nbr == 2 then
		vim.cmd("colorscheme duskfox")
	elseif theme_nbr == 3 then
		vim.g.catppuccin_flavour = "mocha"
		theme.setup({
			compile = {
				enable = true,
			},
			integrations = {
				dap = {
					enable = true,
				},
			},
		})
		vim.cmd("colorscheme catppuccin")
	elseif theme_nbr == 4 then
		vim.g.tokyonight_style = "night"
		vim.cmd("colorscheme " .. themes[theme_nbr])
	else
		vim.g.material_style = "darker"
		vim.cmd("colorscheme " .. themes[theme_nbr])
	end
	require("lualine").setup({
		options = { theme = themes[theme_nbr] },
	})
end

pcall(_setup_theme)

require("nvim-treesitter.configs").setup({
	-- A list of parser names, or "all"
	ensure_installed = { "lua", "rust" },
	sync_install = false,
	auto_install = true,
	ignore_install = { "javascript" },

	highlight = {
		enable = true,
	},
	indent = {
		enable = true,
	},
})

require("lsp_lines").setup()
vim.diagnostic.config({
	virtual_text = false,
	update_in_insert = true,
})

vim.cmd([[highlight IndentBlanklineIndent1 guifg=#E06C75 gui=nocombine]])
vim.cmd([[highlight IndentBlanklineIndent2 guifg=#E5C07B gui=nocombine]])
vim.cmd([[highlight IndentBlanklineIndent3 guifg=#98C379 gui=nocombine]])
vim.cmd([[highlight IndentBlanklineIndent4 guifg=#56B6C2 gui=nocombine]])

require("indent_blankline").setup({
	show_current_context = true,
	show_current_context_start = true,
	char_highlight_list = {
		"IndentBlanklineIndent1",
		"IndentBlanklineIndent2",
		"IndentBlanklineIndent3",
		"IndentBlanklineIndent4",
	},
})
require("nvim-autopairs").setup({})
