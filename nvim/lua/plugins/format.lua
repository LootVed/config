local h = require("null-ls.helpers")
local methods = require("null-ls.methods")

local FORMATTING = methods.internal.FORMATTING
local M = {}

M.anyfmt = h.make_builtin({
	name = "anyfmt",
	meta = {
		url = "https://gitlab.com/lootved/anyfmt",
		description = "Minimal code formatter written in go can be\
    used to quickly format any C like language.",
	},
	method = FORMATTING,
	filetypes = { "groovy", "json", "conf", "cfg" },
	generator_opts = {
		command = "anyfmt",
		to_stdin = true,
	},
	factory = h.formatter_factory,
})

return M
