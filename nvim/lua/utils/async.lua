local M = {}

function M.on_event(job_id, data, event)
	--if event == "stdout" or event == "stderr" then
	--end
end

function M.run(cmd, job_ids)
	local job_id = vim.fn.jobstart(cmd, {
		on_stderr = M.on_event,
		on_stdout = M.on_event,
		on_exit = M.on_event,
		stdout_buffered = true,
		stderr_buffered = true,
	})
	if type(job_ids) == "table" then
		table.insert(job_ids, job_id)
	end
	return job_id
end

function M.wait(maybe_job_ids)
	local job_ids = {}
	if type(maybe_job_ids) == "table" then
		job_ids = maybe_job_ids
	elseif type(maybe_job_ids) == "number" then
		table.insert(job_ids, maybe_job_ids)
	end
	vim.fn.jobwait(job_ids)
end

return M
