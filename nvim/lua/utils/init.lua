function prequire(pack)
	return pecall(require, pack)
end

function pecall(...)
	local ok, val = pcall(...)
	if not ok then
		print("Error while calling ", ...)
		print(val)
	end
	return ok, val
end

function tprint(tbl, indent)
	local formatting, tp
	if not indent then
		indent = 0
	end
	for k, v in pairs(tbl) do
		tp = type(v)
		formatting = string.rep("  ", indent) .. k .. ": "
		if tp == "table" then
			print(formatting)
			tprint(v, indent + 1)
		elseif tp == "boolean" then
			print(formatting .. tostring(v))
		elseif tp == "function" then
			print(formatting .. "function")
		elseif tp == "string" or tp == "number" then
			print(formatting .. v)
		end
	end
end

function is_dev_env()
	local varname = "__NVIM_IS_DEV_ENV"
	return os.getenv(varname) ~= nil
end
